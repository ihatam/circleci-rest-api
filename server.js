console.log("Start normal log","\n")
console.log("Date:",Date(Date.now()),"\n")
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
const dbConnection = require('./db.connection');
//dbConnection.connectToAtlasDb()
app.use(cors())
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}))
// parse requests of content-type - application/json
app.use(bodyParser.json())
// link our route
require('./app/routes/users.route.js')(app)
dbConnection.directConnection()
app.get('/', (req,res) => {
    res.json({"message": "Welcome !"});
})
app.listen(process.env.PORT || 3000,() => {
    console.log('Server is listening on port', process.env.PORT || 3000 )
})
 