const mongoose = require('mongoose');
const dbConfig = require('./config/database.config.js');
var errHandler = function(err){
    console.log("Error while fetchig db url : ",err)
}
module.exports.connectToAtlasDb = async ()=>{
    const db_url = dbConfig.url
    if(db_url == 'null'){
        await dbConfig.dbUrl.then(result => {
            let data = JSON.parse(result)
            connect(data.DB_URL)
        }).catch(err => errHandler(err))
    }else{
        connect(db_url)
    }
}

module.exports.disconnect = async () =>{
    mongoose.disconnect()
    console.log("Succesfully disconnect to dataabse")
}
module.exports.directConnection = () => {
    let url = 'mongodb+srv://mat:matahi@cluster0-2maes.mongodb.net/test?retryWrites=true'
    connect(url)
}
function connect(url){
    mongoose.Promise = global.Promise;
    mongoose.connect(url,{
        useNewUrlParser: true
    }).then(() => {
        console.log("Succesfully connect to dataabse")
    }).catch(err => {
        console.log("Fail to connect to database url was:",url," error : ",err)
    })
}
