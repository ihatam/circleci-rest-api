module.exports = (app) => {
    const users = require('../controllers/users.controller.js');
    const jwtCheck = require('../../tokenChecker.js');

    // Create a new User
    app.post('/singup',users.create );

   // Send login information
    app.post('/login',users.login)

    // Retrieve all users
    app.get('/users',jwtCheck.verifyToken, users.getAllUsers);

    // Retrieve a single users with _id
    app.get('/users/:_id',jwtCheck.verifyToken, users.getUserById);

    // Update a users with _id
    app.put('/users/:_id',jwtCheck.verifyToken, users.update);

    // Delete a Note with _id
    app.delete('/users/:_id',jwtCheck.verifyToken, users.delete);
}