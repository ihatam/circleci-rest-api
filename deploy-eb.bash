#!/bin/bash
apt-get -y -qq update
apt-get -y install python-pip python-dev build-essential
pip install awsebcli --upgrade
mkdir ~/.aws
touch ~/.aws/config
chmod 600 ~/.aws/config
echo "[profile eb-cli]" > ~/.aws/config
echo "aws_access_key_id=$1" >> ~/.aws/config
echo "aws_secret_access_key=$2" >> ~/.aws/config
eb deploy -v --staged