module.exports =  class fakeExpressReqAndRes {
    constructor() {
        this.req = {
            params:{},
            body: {},
            headers: {},
        };
        this.res = {
            statusCalled: 200,
            sendCalledWith: {},
            send: function(arg) { 
                this.sendCalledWith = arg;
            },
            status: function(args){
                this.statusCalled = args
                return this
            },
        }
    }
    compareTo(user){
        if(this.res.sendCalledWith._id == user._id &&
            this.res.sendCalledWith.email == user.email &&
            this.res.sendCalledWith.password == user.password &&
            this.res.sendCalledWith.first_name == user.first_name &&
            this.res.sendCalledWith.last_name == user.last_name &&
            this.res.sendCalledWith.status === user.status){
                return true
        }else{
            return false
        }
    }
}
