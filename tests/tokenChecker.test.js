const expect = require('chai').expect;
const tokenChecker = require('../tokenChecker').verifyToken;
const fakeExpressReqAndRes = require('./fakeClass/fakeExpress')
const tokenConfig = require('../config/token.config');
const jwt = require('jsonwebtoken')

const payload ={subject:1000}
const token = jwt.sign(payload,tokenConfig.secret)

describe('******Token checker*******', function() {
    describe('Check authorization', function() {    
        it('Unautorized request if headers.authorization',function() {
            const reqAndRes = new fakeExpressReqAndRes();
            let result = false
            let req = reqAndRes.req
            let res = reqAndRes.res
            let next = function(){result = true}
            tokenChecker(req,res,next)
            expect(res.statusCalled).to.equal(401)
            expect(res.sendCalledWith.message).to.equal('Unautorized request')
            expect(result).to.be.false
        })
        it('Unautorized request if headers.authorization does not have a token',function() {
            const reqAndRes = new fakeExpressReqAndRes();
            let result = false
            let req = reqAndRes.req
            let res = reqAndRes.res
            req.headers.authorization = `Bearer null`
            let next = function(){result = true}
            tokenChecker(req,res,next)            
            expect(res.statusCalled).to.equal(401)
            expect(res.sendCalledWith.message).to.equal('Unautorized request')
            expect(result).to.be.false
        })
        it('Autorized request if valid token',function() {
            const reqAndRes = new fakeExpressReqAndRes();
            let req = reqAndRes.req
            let res = reqAndRes.res
            let result = false
            req.headers.authorization = `Bearer ${token}`
            let next = function(){result = true}
            tokenChecker(req,res,next)
            expect(res.statusCalled).to.equal(200)
            expect(result).to.be.true
        })
    })
})