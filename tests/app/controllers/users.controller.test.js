const expect = require('chai').expect;
const assert = require('chai').assert;
const usersController = require('../../../app/controllers/users.controller');
const dbConnection = require('../../../db.connection');
const fakeExpressReqAndRes = require('../../fakeClass/fakeExpress')
function checkArray(array){    
    if(Object.keys(array).length >= 1){
        return true
    }else{return false}
}
describe('******Users Controller*******', function() {
    describe('-getAllUsers() function :', function() {    
        it('Should return an array of users', async function() {
            const reqAndRes = new fakeExpressReqAndRes();
            let req = reqAndRes.req
            let res = reqAndRes.res
            try {
                await dbConnection.directConnection()
                await usersController.getAllUsers(req,res)
                const isDataRetrieve = await checkArray(res.sendCalledWith)
                await dbConnection.disconnect()
                expect(isDataRetrieve).to.be.true
                expect(res.sendCalledWith).to.be.an('array','res is an array of user')
            }
            catch (err) {
                console.log(err)
            }
        })
    })
    describe('getUserById() function',function() {    
        it('should return admin user', async function() {
            //Var init
            const reqAndRes = new fakeExpressReqAndRes();
            let req = reqAndRes.req
            let res = reqAndRes.res
            req.params._id = "5c40fc3b633dfd40bed092ec"

            //expected value
            const expected = {
                "_id": "5c40fc3b633dfd40bed092ec",
                "email": "admin@admin.com",
                "password": "20e0cbcd3cf233b748ebc24193b9afa7bfd8636b",
                "first_name": "Jack",
                "last_name": "LOPIU",
                "status": "off",
                "__v": 0
            }
            try {
                await dbConnection.directConnection()
                await usersController.getUserById(req,res)
                await dbConnection.disconnect()
                expect(reqAndRes.compareTo(expected)).to.be.true
            }
            catch (err) {
                console.log(err)
            }
        })
    })
})