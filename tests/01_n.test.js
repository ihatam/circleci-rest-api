const expect = require('chai').expect;

let req = {
    body:"test",
};
describe('Test', function() {
    describe('Test 01 setup', function() {
        it('error if test not started ', function() {
            expect(req.body).to.contain('test');
        });
    })
})